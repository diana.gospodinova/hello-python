import myname
import interview

name = myname.get_name()
print("Hello {} :)".format(name))

print()

details = interview.get_interview_details()
interviewer = details[0]
applicant = details[1]
time = details[2]
print(interviewer, "will interview", applicant, "at", time)
