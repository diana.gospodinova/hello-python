'''Illustrate input and print.'''


def get_interview_details():
    details = []
    interviewer = input("Enter the interviewer's name: ")
    details.append(interviewer)
    applicant = input("Enter the applicant's name: ")
    details.append(applicant)
    time = input("Enter the appointment time: ")
    details.append(time)
    return details
